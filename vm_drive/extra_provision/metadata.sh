echo "Removing node 12"
sudo apt-get -y purge nodejs
sudo rm -rf /usr/lib/node_modules/npm/lib
sudo rm -rf //etc/apt/sources.list.d/nodesource.list
# Install Node.js v11.x
#curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
#sudo apt-get install -y nodejs

# Install Node.js v10.x
#curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
#sudo apt-get install -y nodejs

echo "Installing node 8"
#Install 8.x node version
url -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

echo "Installing exif tool and other dependencies"
sudo apt-get -y \
    -o Dpkg::Options::="--force-confdef" \
    -o Dpkg::Options::="--force-confold" \
    install nodejs-dev node-gyp libssl1.0-dev npm libimage-exiftool-perl

mongo test /var/www/clarodrive-patches/vm_drive/extra_provision/mongo_metadata.js