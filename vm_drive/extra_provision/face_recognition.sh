sudo apt install cmake memcached libmemcached-tools libsm6 libxext6 libxrender-dev python3-pip python3-dev nginx supervisor memcached python3-venv git build-essential libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev python3-dev python3-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev python3-magic libqt4-dev libbz2-dev build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libheif-dev libde265-dev x265

mongo Image_Processing /var/www/clarodrive-patches/vm_drive/extra_provision/mongo_face_recognition.js

sudo /var/www/clarodrive-face-recognition/ virtualenv -p python3 env
source /var/www/clarodrive-face-recognition/env/bin/activate
pip3 install boto3 botocore dlib gunicorn Flask imutils joblib matplotlib numpy opencv-contrib-python pandas pandocfilters pika Pillow pymongo scikit-image scikit-learn scipy seaborn structlog tqdm python-json-logger celery pymemcache flower requests tensorflow torch torchvision opencv-contrib-python pyheif python-magic mahotas