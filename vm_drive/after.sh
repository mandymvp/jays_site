#!/bin/sh

#########################################
location="/var/www/vm_drive/extra_provision/"
#########################################

echo "STARTING AFTER-BASH. PROVISIONING VM"

sudo apt-get -y install python3-pip supervisor
pip3 install -U Flask flask-cors pymongo virtualenv

sudo cp ${location}backend.conf /etc/supervisor/conf.d/
sudo mkdir /var/log/backend_jays
sudo supervisorctl reread
sudo service supervisor restart
mongo test ${location}mongo.js
echo "PROVISIONING FINISHED"
