import { TestBed } from '@angular/core/testing';

import { ScoutingService } from './scouting.service';

describe('DashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScoutingService = TestBed.get(ScoutingService);
    expect(service).toBeTruthy();
  });
});
