import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatPaginatorModule,
  MatFormFieldModule,
  MatTableModule,
  MatInputModule,
  MAT_DIALOG_DATA,
  MatSnackBarModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatExpansionModule,
  MatStepperModule,
  GestureConfig,
  MatCardModule,
  MatListModule,
  MatTooltipModule,

} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {PlayerComponent} from './components/player/player.component';
import {MatIconModule, MatChipsModule} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScoutingRoutingModule } from './scouting-routing.module';
import { DashsboardComponent } from './components/dashboard/dashsboard.component';
import {HAMMER_GESTURE_CONFIG} from "@angular/platform-browser";
import { PitcherComponent } from './components/forms/pitcher/pitcher.component';
import { BatterComponent } from './components/forms/batter/batter.component';
import {ControlsModule} from "../controls/controls.module";
import { DescriptionComponent } from './components/forms/description/description.component';
import { PlayerCardComponent } from './components/player-card/player-card.component';


@NgModule({
  declarations: [
      DashsboardComponent,
    PlayerComponent,
    PitcherComponent,
    BatterComponent,
    DescriptionComponent,
    PlayerCardComponent],
  entryComponents: [PlayerComponent, PlayerCardComponent],
  imports: [
    CommonModule,
    FormsModule,
    ScoutingRoutingModule,
    MatSelectModule,
    MatStepperModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatChipsModule,
    ControlsModule,
    MatCardModule,
    MatListModule,
    MatTooltipModule
  ],
  exports: [DashsboardComponent],
  providers: [
    {provide: MAT_DIALOG_DATA, useValue: {},
    }, { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
  ]
})
export class ScoutingModule { }
