import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource, MatPaginator, MatDialog, MatDialogConfig, MatSnackBar, MatSort} from '@angular/material';
import {ScoutingService} from '../../scouting.service';
import {PlayerComponent} from "../player/player.component";
import {NotifierService} from "angular-notifier";
import {PlayerCardComponent} from "../player-card/player-card.component";


@Component({
  selector: 'app-scouting',
  templateUrl: './dashsboard.component.html',
  styleUrls: ['./dashsboard.component.css']
})
export class DashsboardComponent implements OnInit {

  public players: MatTableDataSource<any>;
  private readonly notifier: NotifierService;

  displayedColumns: string[] = ['name', 'team', 'position', 'overall'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
      public dialog: MatDialog,
      private snackBar: MatSnackBar,
      @Inject(ScoutingService) private scouting: ScoutingService
  ) {

    this.scouting._datasource = new MatTableDataSource<any>([]);
    this.players = this.scouting._datasource;

  }

  ngOnInit() {
    this.scouting._datasource.paginator = this.paginator;
    this.getPlayers();
  }

  /**
   * Open new dialog to insert new player data
   */
  addPlayer() {
    this.scouting.setIsPlayerSave(false);
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '90%';
    dialogConfig.maxWidth = '640px';
    dialogConfig.height = '480px';
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {};
    dialogConfig.panelClass = 'custom-dialog-container';

    this.dialog.open(PlayerComponent, dialogConfig);
  }

  /**
   * Get all players stored in database
   */
  getPlayers() {
    this.scouting.getPlayers().subscribe(
        result => {
          if (result['success'] === 1) {
            this.scouting._datasource.data = result['players'];
          } else {
            this.scouting.errorNotification('SOMETHING WRONG RETRIEVING DATA');
          }
        },
        error => {
          this.scouting.errorNotification(error.message);
        }
    );
  }

  /**
   * Table search
   * @param event letters
   */
  searchInTable(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.players.filter = filterValue.trim().toLowerCase();

    if (this.players.paginator) {
      this.players.paginator.firstPage();
    }
  }

  /**
   * Open a model with player information (Player-card)
   * @param playerData data player
   */
  show(playerData) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '90%';
    dialogConfig.maxWidth = '640px';
    dialogConfig.height = '480px';
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = playerData;
    dialogConfig.panelClass = 'custom-dialog-container';

    this.dialog.open(PlayerCardComponent, dialogConfig);

  }


}
