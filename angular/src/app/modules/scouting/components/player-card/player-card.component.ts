import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ScoutingService} from "../../scouting.service";
import {PlayerComponent} from "../player/player.component";

@Component({
  selector: 'app-player-card',
  templateUrl: './player-card.component.html',
  styleUrls: ['./player-card.component.css']
})
export class PlayerCardComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<PlayerCardComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              @Inject(ScoutingService) private scouting: ScoutingService
  ) { }

  ngOnInit() {}

  printGrades(elem) {
    if (!this.data.isBatter && elem.key !== 'overall') {
      return ' Grade:' + elem.value.grade + ' Velo:' + elem.value.velo;
    }
    return ' Grade:'  + elem.value;
  }


  toCapital(word) {
    return word.charAt(0).toUpperCase() + word.slice(1);
  }

  close() {
    this.dialogRef.close();
  }



  save() {
    this.scouting.addPlayer(this.data).subscribe(
        result => {
          if (result['success'] === 1) {
            this.scouting.setIsPlayerSave(true);
            this.close();
            this.scouting.getPlayers().subscribe(data => {
              this.scouting._datasource.data = data['players'];
            });
            this.scouting.successNotification('SCOUTING REPORT SUCCESSFULLY SAVED');

          } else {
            this.scouting.errorNotification('ERROR SAVING SCOUTIN REPORT');

          }

        },
        error => {
          this.scouting.errorNotification('ERROR SENDING INFO');
        }
    );

  }



}
