import {Component, OnInit, Inject} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {teams, positions} from "../../interfaces/player";
import {ScoutingService} from "../../scouting.service";
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {PlayerCardComponent} from "../player-card/player-card.component";


@Component({
    selector: 'app-player',
    templateUrl: './player.component.html',
    styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

    basicFormGroup: FormGroup;
    gradesFormGroup: FormGroup;
    descriptionFormGroup: FormGroup;
    playerPosition = positions;
    pitcher = ['RHP', 'LHP'];
    teams = teams;
    isBatter = true;

    constructor(
        public dialog: MatDialog,
        private _formBuilder: FormBuilder,
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<PlayerComponent>,
        @Inject(ScoutingService) private scouting: ScoutingService
    ) {
    }

    ngOnInit() {
        this.basicFormGroup = this.formBuilder.group({
            team: new FormControl('', [Validators.required]),
            name: new FormControl('', [Validators.required, Validators.pattern('^(?![\\s]+$)[a-zA-Z\\s]*[a-zA-Z]$')]),
            position: new FormControl('', [Validators.required]),
        });

        this.gradesFormGroup = this.formBuilder.group({});
        this.descriptionFormGroup = this.formBuilder.group({});

    }


    /**
     *Close Dialog
     */
    close() {
        this.dialogRef.close();
    }

    /**
     * Set if is a batter or pitcher
     * @param value
     */
    setIsBatter(value) {
        this.isBatter = !this.pitcher.includes(value);
    }

    /**
     * Pitcher form assign
     * @param event Pitcher form
     */
    pitcherFormGroupChange(event) {
        this.gradesFormGroup = event;
    }

    /**
     * Batter form group assign
     * @param event Batter form
     */
    batterFormGroupChange(event) {
        this.gradesFormGroup = event;
    }

    /**
     * Scouting report text form - assign
     * @param event Scouting report text form
     */
    descriptionFormGroupChange(event) {
        this.descriptionFormGroup = event;
    }

    /**
     * Validate if all forms are complete
     */
    validate() {
        const finalForm = {
            basic: this.basicFormGroup,
            grades: this.gradesFormGroup,
            description: this.descriptionFormGroup
        }

        for (const key in finalForm) {
            if (finalForm[key].status !== 'VALID') {
                return false;
            }
            finalForm[key] = finalForm[key].value;
        }
        finalForm['isBatter'] = this.isBatter;

        return finalForm;

    }

    /**
     * Preparate information to send it. Open a new dialog with an information preview.
     */
    reviewForm() {
        const data = this.validate();

        if (data) {

            let gradesOrdered = {};
            if (!data['isBatter']) {
                gradesOrdered = data.grades['pitchesGrades'];
            } else {
                gradesOrdered = data.grades;
            }
            /**Create a new object with a copy of all grades
             *  in order to avoid delete "overall" from the form */
            const gradesFiltered = gradesOrdered.constructor();
            for (const attribute in gradesOrdered) {
                if (gradesOrdered.hasOwnProperty(attribute)) {
                    gradesFiltered[attribute] = gradesOrdered[attribute];
                }
            }
            delete gradesFiltered['overall'];

            const dialogConfig = new MatDialogConfig();

            dialogConfig.width = '90%';
            dialogConfig.maxWidth = '640px';
            dialogConfig.height = '480px';
            dialogConfig.disableClose = false;
            dialogConfig.autoFocus = true;
            dialogConfig.panelClass = 'custom-dialog-container';
            dialogConfig.data = {
                name: data.basic['name'],
                team: data.basic['team'],
                position: data.basic['position'],
                grades: gradesFiltered,
                overall: data.grades['overall'],
                description: data.description['description'],
                isBatter: data['isBatter'],
                review: true
            };

            this.dialog.open(PlayerCardComponent, dialogConfig).afterClosed().subscribe(() => {
                if (this.scouting.getIsPlayerSave()) {
                    this.close();
                    this.scouting.setIsPlayerSave(false);
                }

            });

        } else {
            this.scouting.errorNotification('INFORMATION INCOMPLETE. PLEASE REVIEW YOUR INFORMATION');

        }
    }
}
