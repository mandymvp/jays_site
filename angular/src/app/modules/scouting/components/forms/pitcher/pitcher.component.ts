import {Component, OnInit, Output, EventEmitter, ViewChild, Inject} from '@angular/core';
import {grades, pitchTypes} from '../../../interfaces/player';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {InputCustomChipsComponent} from '../../../../controls/components/input-custom-chips/input-custom-chips';
import {ScoutingService} from '../../../scouting.service';

@Component({
  selector: 'app-pitcher',
  templateUrl: './pitcher.component.html',
  styleUrls: ['./pitcher.component.css']
})
export class PitcherComponent implements OnInit {

  scoutGrades = grades;
  pitches = pitchTypes;
  form: FormGroup;
  pitchesForm: FormGroup;


  @Output() private formGroupChange = new EventEmitter<any>();
  @ViewChild('chips', {static: false}) chips: InputCustomChipsComponent;

  constructor(private formBuilder: FormBuilder,
              private pitchesFormBuilder: FormBuilder,
              @Inject(ScoutingService) private scoutingService: ScoutingService
  ) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
        pitchesGrades: new FormControl(null, [Validators.required]),
        overall: new FormControl('', [Validators.required])
    });

    this.pitchesForm = this.pitchesFormBuilder.group({
      pitch: new FormControl('' ),
      grade: new FormControl('' ),
      velo: new FormControl('' ),

    })
    this.formGroupChange.emit(this.form);
    this.form.statusChanges.subscribe(() => this.formGroupChange.emit(this.form));
  }

  /**
   * Add new (Pitch type - grade - velocity) to the mat-chip
   * @param pitch Pitch type
   * @param grade Pitch grade
   * @param velo Pitch velocity
   */
  addGrade() {
    const pitch = this.pitchesForm.value.pitch
    const grade = this.pitchesForm.value.grade
    const velo = this.pitchesForm.value.velo

    if (pitch && grade && velo) {
      if (velo >= 40 && velo <= 110) {
        this.chips.addChip(pitch, grade, velo);
      } else {
        this.scoutingService.errorNotification('VELOCITY FIELD HAS TO BE A NUMBER IN (40-110) RANGE');
      }
    } else {
      this.scoutingService.errorNotification('PLEASE FILL ALL FIELDS CORRECTLY');
    }
    this.pitchesForm.reset('');

  }


}
