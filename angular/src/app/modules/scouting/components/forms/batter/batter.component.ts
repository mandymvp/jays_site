import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {grades} from '../../../interfaces/player';

@Component({
  selector: 'app-batter',
  templateUrl: './batter.component.html',
  styleUrls: ['./batter.component.css']
})
export class BatterComponent implements OnInit {


  @Output() private formGroupChange = new EventEmitter<any>();

  gradesFormGroup: FormGroup;
  scoutGrades = grades;
  @Input() form: FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      hitting: new FormControl('', [Validators.required]),
      power: new FormControl('', [Validators.required]),
      running: new FormControl('', [Validators.required]),
      fielding: new FormControl('', [Validators.required]),
      throwing: new FormControl('', [Validators.required]),
      overall: new FormControl('', [Validators.required])

    });
    this.formGroupChange.emit(this.form);
  }

}
