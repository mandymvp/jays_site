import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-description',
    templateUrl: './description.component.html',
    styleUrls: ['./description.component.css']
})
export class DescriptionComponent implements OnInit {

    form: FormGroup;
    @Output() private formGroupChange = new EventEmitter<any>();

    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            description: new FormControl('', [Validators.required])
        });
        this.formGroupChange.emit(this.form);
    }

}
