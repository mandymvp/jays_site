import {Injectable} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {BehaviorSubject} from 'rxjs';
import {HttpClient} from "@angular/common/http";
import {NotifierService} from "angular-notifier";

@Injectable({
    providedIn: 'root'
})
export class ScoutingService {

    public _datasource: MatTableDataSource<any>;
    private datasourceObservable: BehaviorSubject<MatTableDataSource<any>>;
    private isPlayerSave = false;

    public notifier: NotifierService;


    urlBase = 'http://ec2-18-217-109-158.us-east-2.compute.amazonaws.com:5000/'

    constructor(
        private http: HttpClient,
        notifierService: NotifierService) {
        this.notifier = notifierService;
    }

    /**
     * Save player service
     * @param formData
     */
    addPlayer(formData) {
        return this.http.post(this.urlBase + 'save', formData);
    }

    /**
     * Get players service
     */
    getPlayers() {
        return this.http.get(this.urlBase + 'get-players');

    }

    /**
     * Pop a successfull event notification
     * @param message
     */
    successNotification(message: string) {
        this.notifier.notify('success', message || 'SUCCESS');
    }

    /**
     * Pop a failed event notification
     * @param message
     */
    errorNotification(message: string) {
        this.notifier.notify('error', message || 'UNEXPECTED ERROR');
    }

    /**
     * Set-aupdate datasource
     * @param value
     */
    set datasource(value: MatTableDataSource<any>) {
        this._datasource = value;
        this.datasourceChange();
    }

    /**
     * Apply-Update datasource
     */
    datasourceChange() {
        this.datasourceObservable.next(this._datasource);
    }

    /**
     * Set if current player data is already saved
     * @param value Boolean
     */
    setIsPlayerSave(value) {
        this.isPlayerSave = value;
    }

    /**
     * Get if current player data is already saved
     */
    getIsPlayerSave() {
        return this.isPlayerSave;
    }
}
