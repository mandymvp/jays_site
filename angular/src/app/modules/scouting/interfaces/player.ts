
export let teams = ['Atlanta Braves',
    'Miami Marlins',
    'New York Mets',
    'Philadelphia Phillies',
    'Washington Nationals',
    'Chicago Cubs',
    'Cincinnati Reds',
    'Milwaukee Brewers',
    'Pittsburgh Pirates',
    'St. Louis Cardinals',
    'Arizona Diamondbacks',
    'Colorado Rockies',
    'Los Angeles Dodgers',
    'San Diego Padres',
    'San Francisco Giants',
    'Baltimore Orioles',
    'Boston Red Sox',
    'New York Yankees',
    'Tampa Bay Rays',
    'Toronto Blue Jays',
    'Chicago White Sox',
    'Cleveland Indians',
    'Detroit Tigers',
    'Kansas City Royals',
    'Minnesota Twins',
    'Houston Astros',
    'Los Angeles Angels',
    'Oakland Athletics',
    'Seattle Mariners',
    'Texas Rangers'];

export let grades = [
    '20', '25', '30', '35', '40', '45', '50', '55', '60', '65', '70', '75', '80'
];

export let pitchTypes = [
    'Changeup',
    'Curveball',
    'Cutter',
    'Eephus',
    'Forkball',
    '4S-Fastball',
    'Knuckleball',
    'Knuckle-curve',
    'Screwball',
    'Sinker',
    'Slider',
    'Splitter',
    '2S-Fastball',
];

export let positions = [
    'C',
    '1B',
    '2B',
    '3B',
    'SS',
    'LF',
    'CF',
    'RF',
    'RHP',
    'LHP',
];








