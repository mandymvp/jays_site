import {Component, ElementRef, forwardRef, Injector, Input, OnInit, ViewChild} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgControl, Validator} from '@angular/forms';
import {MatChipList} from "@angular/material";

@Component({
    selector: 'app-input-custom-chips',
    templateUrl: './input-custom-chips.html',
    styleUrls: ['./input-custom-chips.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InputCustomChipsComponent),
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => InputCustomChipsComponent),
            multi: true
        }
    ]
})
export class InputCustomChipsComponent implements OnInit, ControlValueAccessor, Validator {

    @Input() isInvalid: boolean;
    @ViewChild('chipsval', {static: true}) field: ElementRef;
    @ViewChild('chipList', {static: true}) chipList: MatChipList;


    mapper = {};

    public ngControl: NgControl;

    public propagateChange = (_: any) => {};
    public propagateTouch = () => {};


    constructor(private injector: Injector) {}

    ngOnInit() {
        this.ngControl = this.injector.get(NgControl);

        setTimeout(() => {
            this.chipList.errorState = true;
        }, 5000);

    }

    get chipsKeys() {
        return Object.keys(this.mapper);
    }

    onChange() {
        this.propagateChange(this.mapper);
        this.chipList.errorState = this.chipsKeys.length === 0;
    }

    onFocusin(event) {
        this.propagateTouch();
        this.chipList.errorState = this.chipsKeys.length === 0 && this.ngControl.touched;
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.propagateTouch = fn;
    }

    writeValue(value: any): void {
        this.field.nativeElement.value = value;
    }

    validate(control: FormControl) {
        this.isInvalid = Object.keys(this.mapper).length === 0;
        return this.isInvalid ? {
            invalidMonth: {
                valid: false,
            },
        } : null;
    }

    addChip(pitch, grade, velo) {
        this.mapper[pitch] = {grade, velo};
        this.onChange();
    }

    removeGrade(pitch): void {
        delete this.mapper[pitch];
        this.onChange();
    }

}
