import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputCustomChipsComponent } from './input-custom-chips';

describe('InputCustomChipsComponent', () => {
  let component: InputCustomChipsComponent;
  let fixture: ComponentFixture<InputCustomChipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputCustomChipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputCustomChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
