import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InputCustomChipsComponent} from './components/input-custom-chips/input-custom-chips';
import {MatChipsModule, MatFormFieldModule, MatIconModule} from "@angular/material";



@NgModule({
  declarations: [InputCustomChipsComponent],
  exports: [InputCustomChipsComponent],
  imports: [
    CommonModule, MatIconModule, MatChipsModule, MatFormFieldModule
  ]
})
export class ControlsModule { }
