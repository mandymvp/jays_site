import { NgModule } from '@angular/core';
import {MatButtonModule, MatGridListModule} from "@angular/material";
import {MatDialogModule} from '@angular/material/dialog';
import {HttpClientModule} from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {RouterModule} from "@angular/router";
import {ScoutingModule} from "./modules/scouting/scouting.module";
import { NotifierModule } from "angular-notifier";



@NgModule({
  declarations: [
    AppComponent

  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatGridListModule,
    RouterModule, MatDialogModule, HttpClientModule, ScoutingModule, NotifierModule],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
