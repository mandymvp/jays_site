FROM node:10 
COPY . .
WORKDIR /angular
RUN npm install -g @angular/cli 
EXPOSE 4200
CMD npm start