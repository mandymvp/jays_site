from pymongo import MongoClient, errors
from environment import MONGODB_CONNECTION


class MongoConnector:
    __connection = None

    def __init__(self):
        self.get_connection()

    def get_connection(self):
        if self.__connection is None:


            connection = "mongodb://{0}:{1}@{2}/{3}".format(MONGODB_CONNECTION.get("user"),
                                                            MONGODB_CONNECTION.get("password"),
                                                            ",".join(MONGODB_CONNECTION.get("hosts")),
                                                            MONGODB_CONNECTION.get("database")
                                                            )
            self.__connection = MongoClient(connection,
                                            replicaSet=MONGODB_CONNECTION.get("replicaSet"),
                                            serverSelectionTimeoutMS=MONGODB_CONNECTION.get("timeout"))



        try:
            self.__connection.server_info()
        except errors.ServerSelectionTimeoutError as ex:
            raise ex

        return self.__connection

    def close_connection(self):
        if self.__connection is not None:
            self.__connection.close()

    def insert(query, col):
        collection = MongoConnector().get_connection()[MONGODB_CONNECTION.get('database')][col]
        inserted_id = collection.insert_one(query).inserted_id
        return inserted_id


