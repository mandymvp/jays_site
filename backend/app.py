from flask import Flask, request
from flask_cors import CORS, cross_origin
from environment import IP_FLASK_SERVER
from players import *

import logging

app = Flask(__name__)
CORS(app)


logging.getLogger('flask_cors').level = logging.DEBUG


@app.route('/', methods=['GET'])
def status():
    """
    Get status info from app
    :return:
    """

    return get_status()

@app.route('/save', methods=['POST'])
def save_data():
    """
    Save players scouting report
    :return:
    """
    print("Llego")


    return save_player(request.json)

@app.route('/get-players', methods=['GET'])
def get_players():
    """
    Save players scouting report
    :return:
    """
    print("Llego")

    return get_all_players()



print("Running Server on {}".format(IP_FLASK_SERVER))
app.run(host=IP_FLASK_SERVER)