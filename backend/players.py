from mongoConnector import MongoConnector as mongodb
from environment import MONGODB_CONNECTION as mongo_info, positions, pitchTypes, grades, teams
import json,traceback
from CustomException import CustomException

INVALID_PARAMETERS_ERROR = 'Invalid parameters'


def get_status():
    try:
        mongodb().get_connection()
        mongo = "Running"
    except Exception as x:
        print(x)
        mongo = "Down"

    return json.dumps({
        'app': 'Scouting players',
        'status': 'Running',
        'mongo': mongo})


def save_player(data):
    try:
        print(data)
        if (validate(data)):
            collection = mongodb().get_connection()[mongo_info.get('database')][mongo_info.get('players_collection')]
            new_player = {
                'name': data['name'],
                'team': data['team'],
                'position': data['position'],
                'grades': data['grades'],
                'overall': data['overall'],
                'description': data['description'],
                'isBatter':data['isBatter']
            }
            collection.insert_one(new_player).inserted_id

    except Exception as ex:
        print("An error occurrea: {}".format(traceback))
        return json.dumps({
            'success': 0,
            'error': ex.args[0]})

    return json.dumps({
        'success': 1})


def validate(data):
    if (data['name'] is None)\
            or (data['team'] not in teams)\
            or (data['position'] not in positions)\
            or (data['description'] is None
            or data['overall'] not in grades):
        raise CustomException("Invalid basic parameters")
    validate_grades_form(data['grades'], data['isBatter'])
    validate_description(data['description'])
    return True


"""def validate_basic_form(basic_info):
    if len(basic_info) == 3:
        if (basic_info['name'] is not None) and (basic_info['team'] in teams) and (basic_info['position'] in positions):
            return True
    raise CustomException(INVALID_PARAMETERS_ERROR)"""


def validate_grades_form(grades_info, isBatter):
    if isBatter:
        for grade in grades_info:
            if grades_info[grade] not in grades:
                raise CustomException(INVALID_PARAMETERS_ERROR)
    else:
        for pitch in grades_info:
            if pitch in pitchTypes:
                if grades_info[pitch]['grade'] not in grades or (int(grades_info[pitch]['velo'])>=40 and int(grades_info[pitch]['velo'])>=110):
                    raise CustomException(INVALID_PARAMETERS_ERROR)
            else:
                raise CustomException(INVALID_PARAMETERS_ERROR)

    return True


def validate_description(description):
    if description is not None:
        return True
    raise CustomException(INVALID_PARAMETERS_ERROR)


def get_all_players():
    try:
        collection = mongodb().get_connection()[mongo_info.get('database')][mongo_info.get('players_collection')]
        players_cursor = collection.find({})

        players = []
        for player in players_cursor:
            del player['_id']
            players.append(player)

        return json.dumps({
            'success': 1,
            'players': players})
    except Exception as ex:
        return json.dumps({
            'success': 0,
            'error': ex.args[0]})


# save_player({'basic': {'team': 'Atlanta Braves', 'name': 'El bateador', 'position': 'CF'}, 'grades': {'hitting': '25', 'power': '30', 'running': '35', 'fielding': '35', 'throwing': '40', 'overall': '45'}, 'description': {'description': 'asnkasfhachnaivsdilcamhsdncndfacskdfkasvfkdfsadfascfnsadnfhcsfhcamshfohasocfhoshxo,adishmcgoaihgoiahsxoighasoighaoñsmhmgaoshnishucfinasufpdsachfinpu'}, 'isBatter': True}
# )

# pitcher
#save_player({'name': 'Primer Pitcher', 'team': 'Atlanta Braves', 'position': 'RHP', 'grades': {'Curveball': {'grade': '30', 'velo': '54'}, 'Splitter': {'grade': '30', 'velo': '54'}, 'Four-Seam Fastball': {'grade': '55', 'velo': '87'}, 'overall': '55'}, 'description': 'Though he pitched with slightly diminished stuff last season due to injury, Gore still showed the makings of developing a front-of-the-rotation-caliber arsenal. His fastball sits at 93-95 mph and can touch 96, often playing above its velocity due to its late life. He has a pair of breaking balls in a mid-70s curve, a plus pitch and nets him plenty of whiffs, and a hard slider in the low 80s. The Padres expect both offerings to be sharper as Gore distances himself from the blister issue. He rounds out his arsenal with a tumbling changeup that has plus potential, and everything he throws plays up due to his big, athletic leg kick and because he gets good extension toward the plate, both of which help create a deceptive look for hitters on both sides of the plate.\n\nGore is lauded for his competitiveness and overall aptitude on the mound. Those qualities, along with his potential for four above-average-or-better offerings, could make the young lefty a future front-of-the-rotation piece. Already on his way toward achieving that projection, Gore stands to rise quickly through the Minors compared to his Draft-class peers, as long as he stays healthy.', 'isBatter': False})

#save_player({'name': 'Primer Bateador', 'team': 'Miami Marlins', 'position': '3B', 'grades': {'hitting': '25', 'power': '25', 'running': '30', 'fielding': '30', 'throwing': '25', 'overall': '35'}, 'description': "Lux broke out offensively after getting stronger and using his legs more while adding more launch angle to his left-handed stroke. Those changes as well as his bat speed give him more power than most middle infielders. He's an advanced hitter who has controlled the strike zone well and made consistent contact throughout his pro career, even when he wasn't putting up big numbers.", 'isBatter': True})
#get_all_players()
